﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ServicosAcademicos.Helpers
{
    public static class ViewHelper
    {
        public static Dictionary<PropertyInfo, bool> map<T>(IEnumerable<T> model)
        {
            Dictionary<PropertyInfo, bool> showList = new Dictionary<PropertyInfo, bool>();

            if (model != null && model.Count() > 0)
            {
                IList<PropertyInfo> props = new List<PropertyInfo>(model.First().GetType().GetProperties());
                foreach (PropertyInfo prop in props)
                {
                    PropertyInfo key = prop;
                    bool value = false;

                    foreach (var item in model)
                    {
                        var propValue = prop.GetValue(model.First(), null);

                        if (propValue != null)
                        {
                            if (propValue.GetType() == typeof(string))
                            {
                                if (!string.IsNullOrWhiteSpace((string)propValue) && (string)propValue != "0")
                                {
                                    value = true;
                                }
                            }
                            else if (propValue.GetType() == typeof(int))
                            {
                                if ((int)propValue != 0)
                                {
                                    value = true;
                                }
                            }
                            else if (propValue.GetType() == typeof(decimal))
                            {
                                if ((decimal)propValue != 0)
                                {
                                    value = true;
                                }
                            }
                            else if (propValue.GetType() == typeof(float))
                            {
                                if ((float)propValue != 0)
                                {
                                    value = true;
                                }
                            }
                            else if(propValue.GetType() == typeof(DateTime))
                            {
                                if ((DateTime)propValue != new DateTime(1, 1, 1))
                                {
                                    value = true;
                                }
                            }
                            else
                            {
                                value = true;
                            }

                            if (value)
                            {
                                break;
                            }
                        }
                    }

                    showList.Add(key, value);
                }
            }

            //ordenar dicionario
            showList.OrderBy(s => toOrder(s.Key));

            return showList;
        }

        public static string toShow<T>(T value)
        {
            string result = string.Empty;

            if (value != null)
            {
                switch (value.GetType().Name)
                {
                    case "DateTime":
                        DateTime date = Convert.ToDateTime(value);
                        if (date != new DateTime(1,1,1)) result = date.ToString("dd/MM/yyyy");
                        break;
                    case "Boolean":
                        result = Convert.ToBoolean(value) ? "Sim" : "Não";
                        break;
                    default:
                        result = value.ToString();
                        break;
                }
            }

            return result;
        }

        public static string toDisplayName(PropertyInfo info)
        {
            string result = string.Empty;

            try
            {
                result = info.GetCustomAttribute<DisplayAttribute>().Name;
            }
            catch { }

            return result;
        }

        public static int toOrder(PropertyInfo info)
        {
            int result = 0;

            try
            {
                result = info.GetCustomAttribute<DisplayAttribute>().Order;
            }
            catch { }

            return result;
        }
    }
}