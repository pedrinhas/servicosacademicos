﻿using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ServicosAcademicos.Helpers
{
    public class RESTService
    {
        readonly string uri;

        public RESTService(string uri)
        {
            this.uri = uri;
        }

        public async Task<string> getUser()
        {
            using (var handler = new HttpClientHandler { Credentials = new NetworkCredential(ConfigurationManager.AppSettings["rest_user"], ConfigurationManager.AppSettings["rest_pass"]) })
            {
                using (HttpClient httpClient = new HttpClient(handler))
                {
                    return await httpClient.GetStringAsync(uri);
                }
            }
        }
    }
}