﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ServicosAcademicos.Models.Requerimentos
{
    public static class RequerimentoWS
    {
        public static List<wsRequerimentos.sigAcadDocument> ListaDocumentos(string idGesDoc, string user, string key)
        {
            wsRequerimentos.GdWFReqAcadServiceClient ws = new wsRequerimentos.GdWFReqAcadServiceClient();

            wsRequerimentos.acadResultListObjects result = ws.wsReqAcadListaDocumentos(idGesDoc, user, key);

            if (result.code == "Ok")
                return result.handles.ToList();
            else
                return null;
        }

        public static List<wsRequerimentos.FileVersionsItem> GetFileList(string numero, string user, string key)
        {
            wsRequerimentos.GdWFReqAcadServiceClient ws = new wsRequerimentos.GdWFReqAcadServiceClient();

            wsRequerimentos.acadResultListarDocumentos result = ws.wsReqListarDocumentos(numero, user, key);

            if (result.code == "Ok")
                return result.list.ToList();
            else
                return null;
        }

        public static Stream GetFile(string numero, string filename, string version, string user, string key)
        {
            wsRequerimentos.GdWFReqAcadServiceClient ws = new wsRequerimentos.GdWFReqAcadServiceClient();

            return ws.wsReqGetFile(numero, filename, version, user, key);
        }

        public static List<RequerimentoSimple> GetRequerimentos(string numero)
        {
            wsRequerimentos.GdWFReqAcadServiceClient ws = new wsRequerimentos.GdWFReqAcadServiceClient();

            List<wsRequerimentos.RequerimentoSimple> result = ws.wsReqListarRequerimentos(numero, "", "").ToList();

            List<RequerimentoSimple> listData = new List<RequerimentoSimple>();

            foreach (var r in result)
            {
                RequerimentoSimple rs = new RequerimentoSimple
                {
                    assunto = r.assunto,
                    data = r.data,
                    estado = r.estado,
                    estadoSigla = r.estadoSigla,
                    texto = r.texto,
                    tipo = r.tipo == "-1" ? "Não Classificado" : r.tipo,
                    email = r.email,
                    idGesDoc = r.idGesDoc              
                };

                listData.Add(rs);
            }

            return listData;
        }

    }
}