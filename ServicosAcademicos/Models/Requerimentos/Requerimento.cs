﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ServicosAcademicos.Models.Requerimentos
{
    public class Requerimento
    {
        private const string errorMessage = "Campo de preenchimento obrigatório.";

        [Display(Name = "Número")]
        public string numero { get; set; }

        [Display(Name = "Nome")]
        public string nome { get; set; }

        public string idMatricula { get; set; }

        public string nomeCurso { get; set; }

        public string codCurso { get; set; }

        [Display(Name = "Curso")]
        public string curso { get { return string.Format("{0} - {1}", codCurso, nomeCurso); } }

        [Display(Name = "Assunto")]
        [Required(ErrorMessage = errorMessage)]
        [DataType(DataType.Text)]
        public string assunto { get; set; }

        [Display(Name = "Mensagem")]
        [Required(ErrorMessage = errorMessage)]
        [DataType(DataType.MultilineText)]
        public string mensagem { get; set; }

        public string idRequerimento { get; set; }

        [Display(Name = "Referência")]
        public string refRequerimento { get; set; }

        [Display(Name = "Data")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy (HH:mm)}")]
        public DateTime data { get; set; }

        [Display(Name = "Estado")]
        public string estado { get; set; }

        public string estadoSigla { get; set; }

        public int iTypeID { get; set; }

        public string tipo { get; set; }

        public string email { get; set; }

        public bool emailSet { get; set; }

        public bool permiteNovoRequerimento { get; set; }
        public string idGesDoc { get; set; }
    }

    public class RequerimentoSimple
    {

        [Display(Name = "Assunto")]
        public string assunto { get; set; }

        [Display(Name = "Texto")]
        public string texto { get; set; }

        [Display(Name = "Data")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy (HH:mm)}")]
        public DateTime data { get; set; }

        [Display(Name = "Estado")]
        public string estado { get; set; }

        [Display(Name = "Estado")]
        public string estadoSigla { get; set; }

        [Display(Name = "Tipo")]
        public string tipo { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        public long idGesDoc { get; set; }
    }
}