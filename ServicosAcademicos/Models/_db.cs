﻿using AcadUNAssemblies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ServicosAcademicos.Models
{
    public static class _db
    {
        private static string ConnectionStringUNorte = ConfigurationManager.ConnectionStrings["dbUNorte"].ConnectionString;

        public static string GetB64(string instituicao)
        {
            string result = string.Empty;

            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringUNorte))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "Instituicao_GetAccessB64";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar).Value = instituicao;

                        con.Open();

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            result = Convert.ToString(reader["accessB64"]);
                        }
                    }

                    con.Close();
                }
            }
            catch { }

            return result;
        }

        public static List<Instituicao> Instituicao_LS()
        {
            List<Instituicao> listData = new List<Instituicao>();

            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringUNorte))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "Instituicao_Acad_LS";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        con.Open();

                        SqlDataReader reader = cmd.ExecuteReader();

                        while(reader.Read()){
                            Instituicao item = new Instituicao();

                            item.Sigla = Convert.IsDBNull(reader["sigla"]) ? string.Empty : Convert.ToString(reader["sigla"]).Trim();
                            item.Descricao = Convert.IsDBNull(reader["descricao"]) ? string.Empty : Convert.ToString(reader["descricao"]).Trim();
                            item.Endpoint = Convert.IsDBNull(reader["endpointAcad"]) ? string.Empty : Convert.ToString(reader["endpointAcad"]).Trim();
                            item.Username = Convert.IsDBNull(reader["accessUsername"]) ? string.Empty : Convert.ToString(reader["accessUsername"]).Trim();
                            item.Password = Convert.IsDBNull(reader["accessPassword"]) ? string.Empty : Convert.ToString(reader["accessPassword"]).Trim();

                            listData.Add(item);
                        }
                    }

                    con.Close();
                }
            }
            catch (Exception e)
            {
            }

            return listData;
        }

        public static string GetMensagem(string chave)
        {
            string result = string.Empty;

            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionStringUNorte))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "Mensagem_Get";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@ws", System.Data.SqlDbType.VarChar).Value = ConfigurationManager.AppSettings["ws"].ToString();
                        cmd.Parameters.Add("@chave", System.Data.SqlDbType.VarChar).Value = chave;

                        con.Open();

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            result = Convert.ToString(reader["mensagem"]);
                        }
                    }

                    con.Close();
                }
            }
            catch { }

            return result;
        }


    }
}