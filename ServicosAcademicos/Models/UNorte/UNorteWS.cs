﻿using AcadUNNamespace.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web;

namespace ServicosAcademicos.Models.UNorte
{
    public static class UNorteWS
    {
        private static List<T> Get<T, IT>(List<IT> listData) where T : new()
        {
            List<T> returnData = new List<T>();
          
            //Load Properties
            IList<PropertyInfo> props = new List<PropertyInfo>(typeof(T).GetProperties());
            IList<PropertyInfo> propsUM = new List<PropertyInfo>(typeof(IT).GetProperties());

            //Set values
            foreach (var item in listData)
            {
                T returnItem = new T();

                foreach (var prop in props)
                {
                    prop.SetValue(returnItem, propsUM.Single(x => x.Name == prop.Name).GetValue(item));
                }

                returnData.Add(returnItem);
            }

            return returnData;
        }

        public static bool TestConnection(string instituicao)
        {
            bool result = false;

            try
            {
                if (instituicao == "UM")
                {
                    UMAcad.iinformacao_academicaClient client = new UMAcad.iinformacao_academicaClient();

                    using (new OperationContextScope(client.InnerChannel))
                    {
                        // Add a HTTP Header to an outgoing request
                        //https://stackoverflow.com/questions/18886660/how-to-add-http-header-to-soap-client
                        HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                        requestMessage.Headers["Authorization"] = _db.GetB64(instituicao);
                        OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;

                        result = client.getEstadoWS();
                    }
                }
            }
            catch { }

            return result;
        }
        
        public static List<Curso> GetCursos(string instituicao)
        {
            List<Curso> returnList = new List<Curso>();

            if(instituicao == "UM")
            {
                UMAcad.iinformacao_academicaClient client = new UMAcad.iinformacao_academicaClient();               

                List<UMAcad.Curso> listData = new List<UMAcad.Curso>();

                //Add headers to soap client                
                using (new OperationContextScope(client.InnerChannel))
                {
                    // Add a HTTP Header to an outgoing request
                    HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                    requestMessage.Headers["Authorization"] = _db.GetB64(instituicao);
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;

                    listData = client.getListCursos().ToList();
                }

                client.Close();


                returnList = Get<Curso, UMAcad.Curso>(listData);
            }

            return returnList;
        }

        public static List<Aluno> GetAlunos(string instituicao, string curso)
        {
            List<Aluno> returnList = new List<Aluno>();

            if (instituicao == "UM")
            {
                UMAcad.iinformacao_academicaClient client = new UMAcad.iinformacao_academicaClient();

                List<UMAcad.Aluno> listData = new List<UMAcad.Aluno>();
                
                using (new OperationContextScope(client.InnerChannel))
                {
                    // Add a HTTP Header to an outgoing request
                    HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                    requestMessage.Headers["Authorization"] = _db.GetB64(instituicao);
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;

                    listData = client.getListAlunosPorCurso(curso).ToList();
                }

                client.Close();

                returnList = Get<Aluno, UMAcad.Aluno>(listData);
            }

            return returnList;
        }

        public static List<Plano> GetPlanos(string instituicao, string curso, string anoletivo = "")
        {
            List<Plano> returnList = new List<Plano>();

            if (instituicao == "UM")
            {
                UMAcad.iinformacao_academicaClient client = new UMAcad.iinformacao_academicaClient();

                List<UMAcad.Plano> listData = new List<UMAcad.Plano>();

                using (new OperationContextScope(client.InnerChannel))
                {
                    // Add a HTTP Header to an outgoing request
                    HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                    requestMessage.Headers["Authorization"] = _db.GetB64(instituicao);
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;

                    int intAnoLetivo;
                    if(string.IsNullOrWhiteSpace(anoletivo) || !int.TryParse(anoletivo, out intAnoLetivo))
                        listData = client.getListPlanosPorCurso(curso).ToList();
                    else
                        listData = client.getListPlanosPorCursoPorAnoLetivo(curso, intAnoLetivo).ToList();

                }

                client.Close();

                returnList = Get<Plano, UMAcad.Plano>(listData);
            }

            return returnList;
        }

        public static List<Candidatura> GetCandidaturas(string instituicao, string curso, string anoletivo)
        {
            List<Candidatura> returnList = new List<Candidatura>();

            if (instituicao == "UM")
            {
                UMAcad.iinformacao_academicaClient client = new UMAcad.iinformacao_academicaClient();

                List<UMAcad.CandidaturaCurso> listData = new List<UMAcad.CandidaturaCurso>();

                using (new OperationContextScope(client.InnerChannel))
                {
                    // Add a HTTP Header to an outgoing request
                    HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                    requestMessage.Headers["Authorization"] = _db.GetB64(instituicao);
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;

   
                    listData = client.getListCandidaturasPorCursoPorAnoLetivo(curso, Convert.ToInt32(anoletivo)).ToList();

                }

                client.Close();

                returnList = Get<Candidatura, UMAcad.CandidaturaCurso>(listData);
            }

            return returnList;
        }

        public static List<Inscricao> GetInscricoes(string instituicao, string curso, string anoletivo)
        {
            List<Inscricao> returnList = new List<Inscricao>();

            if (instituicao == "UM")
            {
                UMAcad.iinformacao_academicaClient client = new UMAcad.iinformacao_academicaClient();

                List<UMAcad.InscricaoCurso> listData = new List<UMAcad.InscricaoCurso>();

                using (new OperationContextScope(client.InnerChannel))
                {
                    // Add a HTTP Header to an outgoing request
                    HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                    requestMessage.Headers["Authorization"] = _db.GetB64(instituicao);
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;


                    listData = client.getListInscricoesPorCursoPorAnoLetivo(curso, Convert.ToInt32(anoletivo)).ToList();

                }

                client.Close();

                returnList = Get<Inscricao, UMAcad.InscricaoCurso>(listData);
            }

            return returnList;
        }

        public static List<UnidadeCurricular> GetUnidadesCurriculares(string instituicao, string curso, string plano)
        {
            List<UnidadeCurricular> returnList = new List<UnidadeCurricular>();

            if (instituicao == "UM")
            {
                UMAcad.iinformacao_academicaClient client = new UMAcad.iinformacao_academicaClient();

                List<UMAcad.UnidadeCurricular> listData = new List<UMAcad.UnidadeCurricular>();

                using (new OperationContextScope(client.InnerChannel))
                {
                    // Add a HTTP Header to an outgoing request
                    HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                    requestMessage.Headers["Authorization"] = _db.GetB64(instituicao);
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;


                    listData = client.getListUCsPorCursoPlano(curso, plano).ToList();

                }

                client.Close();

                returnList = Get<UnidadeCurricular, UMAcad.UnidadeCurricular>(listData);
            }

            return returnList;
        }

    }
}