﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServicosAcademicos.Models
{
    public class Linguagem
    {
        public string title { get; set; }
        public string sigla { get; set; }
        public string texto { get; set; }
        public string href { get; set; }

    }
}