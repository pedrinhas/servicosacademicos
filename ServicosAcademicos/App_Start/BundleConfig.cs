﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Optimization;

namespace ServicosAcademicos
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["AlternativeDesign"]))
            {
                bundles.Add(new ScriptBundle("~/bundles/js").Include(
                            "~/Scripts/progressBar.js",
                            "~/Scripts/progressBar.min.js",
                            "~/Scripts/jquery.ui.datepicker-en-GB.js",
                            "~/Scripts/jquery.ui.datepicker-pt-PT.js",
                            "~/Scripts/utils.js"));

                bundles.Add(new StyleBundle("~/Content/css").Include(
                            "~/Content/Site.css"));
            }
        }
    }
}
