﻿$(document).ready(function () {

    var culture = $('#cultureValue').val();

    $.datepicker.setDefaults($.datepicker.regional[culture]);
    $('input[type=date]').datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1950:' + (new Date().getFullYear() + 2).toString()
    }).prop('type', 'text');
    $('input[type=datetime]').datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1950:' + (new Date().getFullYear() + 2).toString()
    }).prop('type', 'text');

    $('.btn-datas').click(function () {
        $(this).closest('.input-group-btn').siblings('input').datepicker('show');
    });

});

function link(page) {
    var url = window.location.protocol + "//" + window.location.host + "/sages/Requerimento/Index" + "?page=" + page;
    var texto = document.getElementById("procurar").value;

    if (texto != "") {
        url += '&texto=' + texto;
    }

    $(location).attr('href', url);

}

function procurar(texto){
    var url = window.location.protocol + "//" + window.location.host + "/sages/Requerimento/Procurar";

    $.post(url, { 'texto': texto }, function (data) {
        if (data != 'error') {
            var list = JSON.parse(data);

            $('table tr').not(function () { return !!$(this).has('th').length; }).remove();

            var tr;
            for (var i = 0; i < list.alunos.length; i++) {
                tr = $('<tr/>');
                tr.append("<td>" + list.alunos[i].numero + "</td>");
                tr.append("<td>" + list.alunos[i].nome + "</td>");
                tr.append("<td><a href=\"/sages/Requerimento/RequerimentosAluno/" + list.alunos[i].numero + "\"><input value=\"Ver\" class=\"btn btn-default\" style=\"float: right;\" type=\"button\"></a></td>");
                $('table').append(tr);
            }

            loadPageNum(list.n_pages, list.actual_page);
        }
    });
}

function loadPageNum(n_pages, actual_page) {
    var element = $("#page_num");

    element.empty();

    if (actual_page > 1)
    {
        if (actual_page > 2)
        {
            element.append("<a title='Primeira página' class=\"btn btn-default\" onclick=\"link(1)\"><<</a>");
        }
        else
        {
            element.append("<a title='Primeira página' class=\"btn btn-default\" disabled><<</a>");
        }
        element.append("<a title='Página anterior' class=\"btn btn-default\" onclick=\"link("+(actual_page - 1)+")\"><</a>");
        element.append("<a title='Página "+(actual_page - 1)+"' class=\"btn btn-default\" onclick=\"link(" + (actual_page - 1) + ")\">" + (actual_page - 1) + "</a>");
    }
    else
    {
        element.append("<a title='Primeira página' class=\"btn btn-default\" disabled><<</a>");
        element.append("<a title='Página anterior' class=\"btn btn-default\" disabled><</a>");
    }
    element.append("<a title='Página actual' class=\"btn btn-primary\" disabled>" + actual_page + "</a>");
    if (n_pages > actual_page)
    {
        element.append("<a title='Página "+(actual_page + 1)+"' class=\"btn btn-default\" onclick=\"link(" + (actual_page + 1) + ")\">" + (actual_page + 1) + "</a>");
        element.append("<a title='Próxima página' class=\"btn btn-default\" onclick=\"link(" + (actual_page + 1) + ")\">></a>");

        if (n_pages > actual_page + 1)
        { 
            element.append("<a title='Última página' class=\"btn btn-default\" onclick=\"link(" + n_pages + ")\">>></a>");
        }
        else
        {
            element.append("<a title='Última página' class=\"btn btn-default\" disabled>>></a>");
        }

        }
    else
    {
        element.append("<a title='Próxima página' class=\"btn btn-default\" disabled>></a>");
        element.append("<a title='Última página' class=\"btn btn-default\" disabled>>></a>");
    }

}