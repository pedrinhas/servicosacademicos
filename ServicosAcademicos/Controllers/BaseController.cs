﻿using System.Collections.Generic;
using System.Web.Mvc;
using ServicosAcademicos.Models;
using ServicosAcademicos.ViewModels;
using System.Configuration;
using ServicosAcademicos.App_Start;
using System.Net;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;
using System.Web;
using System;
using System.Resources;
using ServicosAcademicos.Resources;
using System.Web.Routing;

namespace ServicosAcademicos.Controllers
{
    public class BaseController : Controller
    {
        static string currentLanguage;

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            currentLanguage = System.Web.HttpContext.Current.Session["culture"] != null ? System.Web.HttpContext.Current.Session["culture"].ToString() : Thread.CurrentThread.CurrentUICulture.ToString();
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(currentLanguage);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(currentLanguage);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            try
            {
                base.OnActionExecuted(filterContext);

                BaseViewModel model = new BaseViewModel();

                //inicializar model.aplicaoes
                string urlWS = "";
                if (Autenticacao.IsAnonimo)
                {
                    urlWS = string.Format("{0}{1}/{2}/{3}", ConfigurationManager.AppSettings["AppMenuWSLinguagem"], Autenticacao.GetGrupo, ConfigurationManager.AppSettings["app"], currentLanguage);
                }
                else
                {
                    urlWS = string.Format("{0}{1}/{2}/{3}/{4}", ConfigurationManager.AppSettings["AppMenuWSLinguagem"], Autenticacao.GetGrupo, ConfigurationManager.AppSettings["app"], Autenticacao.Utilizador, currentLanguage);
                }
                WebClient wcWS = new WebClient();
                string data = wcWS.DownloadString(urlWS).ToUTF8();
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                List<Aplicacao> aplicacoes = serializer.Deserialize<List<Aplicacao>>(data);
                model.Aplicacoes = aplicacoes;

                //inicializar model.menu
                List<MenuItem> menu = new List<MenuItem>();

                if (Autenticacao.IsLoggedIn)
                {
                    menu.Add(new MenuItem() { Descricao = "UNorte", Toggle = "UNorte", Url = Url.Action("Index", "UNorteGateway") });
                    menu.Add(new MenuItem() { Descricao = "Requerimentos", Toggle = "Requerimentos", Url = Url.Action("Index", "Requerimento") });
                }
                model.Menu = menu;

                List<Linguagem> linguagens = new List<Linguagem>();
                Linguagem PT = new Linguagem() { title = "Português", sigla = "pt-PT", texto = "PT", href = Url.Action("ChangeLanguage", "Base", new { sigla = "pt-PT" }) };
                linguagens.Add(PT);
                linguagens.Add(new Linguagem() { title = "English", sigla = "en-GB", texto = "EN", href = Url.Action("ChangeLanguage", "Base", new { sigla = "en-GB" }) });

                ViewBag.Language = linguagens.Find(a => a.sigla == currentLanguage) ?? PT;
                linguagens.Remove(ViewBag.Language);

                model.Linguagens = linguagens;

                ViewBag.Titulo = ResourceBase.Titulo;
                
                filterContext.Controller.ViewData["LayoutModel"] = model;

                //verificar erro de autenticação
                if (Session["erroAutenticacao"] != null)
                    TempData["error"] = Session["erroAutenticacao"].ToString();

            }
            catch (Exception ex)
            {
            }
        }

        public ActionResult ChangeLanguage(string sigla)
        {
            System.Web.HttpContext.Current.Session["culture"] = sigla;

            return Redirect(Request.UrlReferrer.AbsoluteUri);
        }
    }
}