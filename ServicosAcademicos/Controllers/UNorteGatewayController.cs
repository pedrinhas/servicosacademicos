﻿using AcadUNAssemblies;
using AcadUNNamespace.Models;
using ServicosAcademicos.App_Start;
using ServicosAcademicos.Models;
using ServicosAcademicos.Models.UNorte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServicosAcademicos.Controllers
{
    public class UNorteGatewayController : BaseController
    {
        void LoadDropdown(string sigla = null)
        {

            var list = _db.Instituicao_LS();

            if (sigla == null)
            {
                sigla = "-";
                list.Insert(0, new Instituicao() { Sigla = "-", Descricao = "Selecione uma instituição" });
            }

            ViewBag.instituicoes = new SelectList(
                                                list,
                                                "Sigla",
                                                "Descricao",
                                                sigla
                                            );
        }

        // GET: UNorteGateway
        [UTADLoggedIn]
        [UTADAdmin]
        public ActionResult Index()
        {
            LoadDropdown();

            return View();
        }

        //cursos por instituicao
        [UTADLoggedIn]
        [UTADAdmin]
        public ActionResult Cursos(string instituicao)
        {
            LoadDropdown(instituicao);

            List<Curso> listData = new List<Curso>();

            if (UNorteWS.TestConnection(instituicao))
            {
                listData = UNorteWS.GetCursos(instituicao).OrderBy(x => x.CodigoDGES).ToList();
            }
            else
            {
                TempData["error"] = string.Format(_db.GetMensagem("SemComunicacao"), instituicao);
            }

            return View("Index", listData);
        }

        //alunos por curso
        [UTADLoggedIn]
        [UTADAdmin]
        public ActionResult Alunos(string instituicao, string curso)
        {
            List<Aluno> listData = new List<Aluno>();
            ViewBag.ParentTitle = string.Empty;

            if (UNorteWS.TestConnection(instituicao))
            {
                listData = UNorteWS.GetAlunos(instituicao, curso).OrderBy(x => x.Nome).ToList();

                ViewBag.ParentTitle = UNorteWS.GetCursos(instituicao).SingleOrDefault(x => x.CodigoDGES == curso).Designacao;
            }
            else
            {
                TempData["error"] = string.Format(_db.GetMensagem("SemComunicacao"), instituicao);
            }

            
            return View(listData);
        }

        //planos por curso e ano letivo
        [UTADLoggedIn]
        [UTADAdmin]
        public ActionResult Planos(string instituicao, string curso, string anoletivo = null)
        {
            List<Plano> listData = new List<Plano>();
            ViewBag.ParentTitle = string.Empty;

            //load planos
            if (UNorteWS.TestConnection(instituicao))
            {
                listData = UNorteWS.GetPlanos(instituicao, curso, anoletivo).OrderBy(x => x.Codigo).ToList();

                ViewBag.ParentTitle = UNorteWS.GetCursos(instituicao).SingleOrDefault(x => x.CodigoDGES == curso).Designacao;
            }
            else
            {
                TempData["error"] = string.Format(_db.GetMensagem("SemComunicacao"), instituicao);
            }

            

            return View(listData);
        }

        //ucs por plano
        [UTADLoggedIn]
        [UTADAdmin]
        public ActionResult UCS(string instituicao, string curso, string plano)
        {
            //load ucs
            List<UnidadeCurricular> listData = new List<UnidadeCurricular>();
            ViewBag.ParentTitle = string.Empty;

            if (UNorteWS.TestConnection(instituicao))
            {
                listData = UNorteWS.GetUnidadesCurriculares(instituicao, curso, plano).OrderBy(x => x.AnoCurricular).ThenBy(x => x.PeriodoAulas).ThenBy(x => x.CreditosECTS).ToList();

                ViewBag.ParentTitle = UNorteWS.GetPlanos(instituicao, curso).SingleOrDefault(x => x.Codigo == plano).Designacao;
            }
            else
            {
                TempData["error"] = string.Format(_db.GetMensagem("SemComunicacao"), instituicao);
            }



            return View(listData);
        }

        //candidaturas por curso e ano letivo
        [UTADLoggedIn]
        [UTADAdmin]
        public ActionResult Candidaturas(string instituicao, string curso, string anoletivo)
        {
            //load candidaturas
            List<Candidatura> listData = new List<Candidatura>();
            ViewBag.ParentTitle = string.Empty;

            if (UNorteWS.TestConnection(instituicao))
            {
                listData = UNorteWS.GetCandidaturas(instituicao, curso, anoletivo).OrderByDescending(x => x.Colocado).ThenBy(x => x.Nome).ToList();

                ViewBag.ParentTitle = UNorteWS.GetCursos(instituicao).SingleOrDefault(x => x.CodigoDGES == curso).Designacao;
            }
            else
            {
                TempData["error"] = string.Format(_db.GetMensagem("SemComunicacao"), instituicao);
            }
            
            return View(listData);
        }

        //inscricoes por curso e ano letivo
        [UTADLoggedIn]
        [UTADAdmin]
        public ActionResult Inscricoes(string instituicao, string curso, string anoletivo)
        {
            //load inscricoes
            List<Inscricao> listData = new List<Inscricao>();
            ViewBag.ParentTitle = string.Empty;

            if (UNorteWS.TestConnection(instituicao))
            {
                listData = UNorteWS.GetInscricoes(instituicao, curso, anoletivo).OrderByDescending(x => x.AnoLetivo).ThenBy(x => x.IdentificadorInterno).ToList();

                ViewBag.ParentTitle = UNorteWS.GetCursos(instituicao).SingleOrDefault(x => x.CodigoDGES == curso).Designacao;
            }
            else
            {
                TempData["error"] = string.Format(_db.GetMensagem("SemComunicacao"), instituicao);
            }
            

            return View(listData);
        }

    }
}