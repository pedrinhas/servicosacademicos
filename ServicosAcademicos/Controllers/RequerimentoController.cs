﻿using ServicosAcademicos.App_Start;
using ServicosAcademicos.Models;
using ServicosAcademicos.Models.Requerimentos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ServicosAcademicos.Controllers
{
    public class RequerimentoController : BaseController
    {
        private static string USER = "RequerimentosOnline";
        private static string KEY = "0000";

        private static int alunos_por_pagina = 12;

        // GET: Requerimento
        [UTADLoggedIn]
        [UTADAdmin]
        public ActionResult Index(int? page, string texto)
        {
            List<AlunoCurso> alunos = string.IsNullOrWhiteSpace(texto) ? db.GetAlunosRequerimentos() : db.GetAlunosRequerimentos(texto);

            int n_pages = (int)(alunos.Count() / alunos_por_pagina) + 1;

            int actual_page = !page.HasValue ? 1 : page.Value;

            ViewBag.NPages = n_pages;
            ViewBag.APage = actual_page;
            ViewBag.Texto = texto ?? "";

            int index = (actual_page - 1) * alunos_por_pagina;

            return View(alunos.GetRange(index, alunos.Count - index < alunos_por_pagina ? alunos.Count - index :  alunos_por_pagina));

            //return View(alunos);

        }

        [UTADLoggedIn]
        [UTADAdmin]
        public ActionResult RequerimentosAluno(int id)
        {
            List<RequerimentoSimple> list = RequerimentoWS.GetRequerimentos(id.ToString()).OrderByDescending(r => r.data).ToList();
            ViewBag.Aluno = string.Format("{0} ({1})", db.GetNomeAlunoCursoAluno(id.ToString()).First().nome, id);

            return View(list);
        }

        //localhost:8910/Requerimento/ConsultaProcesso/1134774453
        [UTADLoggedIn]
        [UTADAdmin]
        public ActionResult ConsultaProcesso(long id)
        {
            List<Requerimento> reqList = db.ReqAcadListaParaProcesso(id, USER, KEY).OrderByDescending(r => r.data).ToList();

            reqList.First().idRequerimento = db.Req_Get_Id_ArqDE(id);

            List<wsRequerimentos.sigAcadDocument> docList = RequerimentoWS.ListaDocumentos(id.ToString(), USER, KEY);

            List<wsRequerimentos.FileVersionsItem> arqdeList = RequerimentoWS.GetFileList(id.ToString(), USER, KEY);

            List<wsRequerimentos.FileVersionsItem> returnList = new List<wsRequerimentos.FileVersionsItem>();

            List<SelectList> versoes = new List<SelectList>();
            if (arqdeList != null)
            {
                foreach (var d in docList)
                {
                    List<wsRequerimentos.FileVersionsItem> files = arqdeList
                        .Where(a => Path.GetFileNameWithoutExtension(a.name) == d.title)
                        .OrderByDescending(a => a.version).ToList();
                    
                    if (files.Any())
                    {
                        wsRequerimentos.FileVersionsItem file = files.First();
                        returnList.Add(file);
                        versoes.Add(new SelectList(
                            files.Select(a => a.version).OrderByDescending(a => a),
                            file.version
                        ));
                    }
                }
            }

            ViewData.Add("versoes", versoes);

            if (returnList != null && returnList.Count > 0)
                ViewData["docs"] = returnList;

            return View(reqList);
        }

        //localhost:8910/Requerimento/Documento/1134774453?handleDoc=97-e-DSA-2017.pdf
        [UTADLoggedIn]
        [UTADAdmin]
        public FileContentResult Documento(long id, string handleDoc, string versao)
        {
            try
            {
                Stream result = RequerimentoWS.GetFile(id.ToString(), handleDoc, versao, USER, KEY);

                using (MemoryStream ms = new MemoryStream())
                {
                    result.CopyTo(ms);

                    return File(ms.ToArray(), MediaTypeNames.Application.Pdf);
                }
            }
            catch (Exception e)
            {
            }

            TempData["error"] = "Ocorreu um erro ao aceder ao documento.";
            return null;
        }

        [UTADLoggedIn]
        [UTADAdmin]
        public JsonResult Procurar(string texto)
        {
            List<AlunoCurso> alunos = db.GetAlunosRequerimentos(texto);

            int n_pages = (int)(alunos.Count / alunos_por_pagina) + 1;

            int actual_page = 1;

            object result = new
            {
                n_pages = n_pages,
                actual_page = actual_page,
                alunos = alunos.GetRange(0, alunos.Count < alunos_por_pagina ? alunos.Count : alunos_por_pagina)
            };

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            JsonResult js = new JsonResult();
            js.Data = serializer.Serialize(result);
            return js;
        }
    }
}