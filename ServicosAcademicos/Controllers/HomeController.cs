﻿using ServicosAcademicos.App_Start;
using System.Configuration;
using System.Web.Mvc;

namespace ServicosAcademicos.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if (!Autenticacao.IsLoggedIn && Session["shib"] == null)
            {
                string utilizadorDevTeste = ConfigurationManager.AppSettings["autenticacaoUsername"].ToString();
                if (!string.IsNullOrWhiteSpace(utilizadorDevTeste))
                    Session["shib"] = utilizadorDevTeste;
            }

            return View();
        }
    }
}